# GitLab Partner Demo Delivery Guide

This repository contains all the resources for partners to successfully deliver a 1-hour-long, high-level, technical overview demo of GitLab.

## Table of Contents

* [Video](https://youtu.be/h8HgSgAoqX8): a tutorial video about how to do a demo.

* [Setup instructions](docs/setup.md): step-by-step instructions for setting up the demo environment seen in the video.

* [Video script](docs/script.md): the full script of the demo part of the video.

* [Slide deck](assets/slides_template.pptx): the slide deck template used in the demo part of the video.

* [Demo recording](https://youtu.be/s4EqNNs3En0): a recording of a demo delivered based on this guide.

## Other assets

All assets used to make the video can be found in [this folder](/assets/video).
