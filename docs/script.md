# Video Script

**Start the video with the slides open.**

Hi everybody, my name is Péter Bozsó, channel solutions architect from GitLab. Some of you who watch this video might already know me. If you don't: my role at GitLab is to enable our partners around the technical side of our product. That's why I am creating this video now. In this video I'd like to do a walkthrough of an approximately 1-hour long demo of GitLab. Of course, these are just guidelines, and you are the ones who know your customers better. So please use this just as it is: a bunch of guidelines and spend time with features you think would be more interesting to your target audience.

Before jumping into the demo itself, let me just quickly talk about how to do a demo. At GitLab, we use the tell-show-tell technique. You usually start with a couple of slides - not more than 3 or 4 -, where you shortly explain what GitLab is, what the demo will be about, what the audience will see and iterate on the key messages around GitLab. Then you switch to your GitLab project and do the actual demo. At the end, you come back to the slides and relate back to what you've shown in the demo and reiterate on the key messages. Another important tip for an effective demo is to make it as interactive as possible. Stop at certain points and make sure the audience is still following you as well as ask questions which could start a conversation regarding certain features. Through these discussions, try to emphasize as much as possible the values GitLab can bring to your customers.

I usually use these couple of slides. We will share this deck with you alongside the video you are watching right now. As you can see, very short, really just the main points and a high-level overview of the GitLab workflow. Make sure your audience understands what GitLab is, why its approach is unique in the DevOps world and what will be covered in the upcoming demo.

**Switch to GitLab.**

Now let's take a look at the actual demo project. We'll provide you with all the information you need to set up such a demo project in your own environment. It's a very simple one, more or less a hello world Node.js application, but complex enough to showcase all the important features of GitLab, especially our Ultimate ones.

Start with a short overview of the UI itself. Show how easy it is to navigate all the different features and iterate on the one application approach. Talk about how having everything development related in one place can reduce the amount of context switches, thus saving time and making your teams more productive. Mention how it also makes hiring and onboarding much easier and faster, because if somebody knows GitLab, they can be productive in your team very fast. Talk about how providing your employees with the highest quality tools helps to retain top talent.

Take some time to show what is a group, what is a project in GitLab and how you can organize your teams and work with them. Show how you can navigate between these two and explain how some features are available both on the group and the project level.

Talk about how this enables top down project management techniques and brings more visibility and clarity into your processes. At the same time, issues can be raised by anyone in your organization, commented directly, and if accepted, code can be contributed and merge accepted. Having planning and code contribution and review in a single application creates a culture where everyone can contribute (even security, infra, other teams, etc.)
 
Show the analytics features and how those can be a single pane of glass into what's happening in your projects. A good example here would be to show GitLab's own analytics dashboard.

**Switch to https://gitlab.com/gitlab-org/gitlab/-/value_stream_analytics**

Here you can also mention the fact that GitLab uses GitLab to develop GitLab.

**Switch back to the demo project.**

Now go to the issue board and talk about how it is a traditional Kanban board that you can customize according to your needs by adding different columns. Mention that in context of GitLab an issue doesn't necessary mean it's a problem, it's just that's how we call the smallest piece of trackable work. Show how easy it is to collaborate through issues, assign people to issues, use labels and track time spent with each task. Iterate on how this brings additional visibility into your processes and reduce the time spent on planning by making communication in your teams more efficient.

Show the merge request connected to this issue. Talk about how issues and merge requests compliment each other nicely in the standard GitLab flow. Explain that issues are for planning and tracking work while merge requests are the place where the actual development work happens. Show how easy it is to navigate between the two. Reiterate on the value of having one single app, one UI and how this facilitates the easy collaboration in the team. Here you can also touch on the new Web IDE and show how easy it is to make changes to your code without even leaving your browser window. Mention how using issues and merge requests together can make GitLab the single source of truth for all your development activities.

Next, talk about the pipelines. Don't get into the technical details of how you create them, just explain that they are now an industry standard in software development and a method for automating your workflows. Stress how much time you can save by automating manual tasks and how you can go to production and deliver new features faster and with greater confidence. Mention Auto DevOps and explain how that unique feature of GitLab can enable customers to quickly hit the ground running with pipelines. Explain that using pipelines for building, testing and deploying your applications increases the visibility of what is happening in your projects. Show the pipelines dashboard and the status of each pipeline run and how a pipeline's detailed view looks like without digging too deep into the details. Show how easily you can access your application running in production and review from the Environments feature.

Connected to the pipelines, touch on the topic of GitLab being fully vendor neutral. Explain that it means that it doesn't matter which cloud provider and deployment technology you use, GitLab will work exactly the same. Touch on the topic of hosting GitLab yourself and how that can also be done in any public cloud as well as on your or your customers' own infrastructure. Talk about how GitLab SaaS and self-managed GitLab instances are running the same version of GitLab and because of that, have the same feature set. Explain that as demonstrated so far, how having one tool for doing everything from planning until deployment into production can make your teams more efficient. Because instead of spending time with maintaining your point-solution tools and the integration between those tools, you can spend time with what actually matters, that is delivering value to your customers.

The rest of the demo should be spent with the security and compliance features. Please note that these features are only available in Ultimate licenses, so skip this part if you are talking to potential Premium customers. If you don't skip it, make sure you walk through at least these three features: vulnerability report, dependency list, license compliance. Explain how these are enabled by pipelines, in this case, automatically by Auto DevOps. Talk about the importance of shifting left and how reporting security and compliance issues early in the development lifecycle can save your customer significant amount of time and money. Here you can talk about some past security breaches that reached even the mainstream media, like the Log4J vulnerability or the SolarWinds hack and point out how using GitLab can protect your organization from making these sometimes very expensive mistakes.

For the vulnerability report, talk about how easy it is to understand each vulnerability and create a new issue to track its remediation, closing the whole DevSecOps loop. Show that you can triage issues. Mention the possibility of exporting this report both from the UI and through the API.

For the dependency list: explain what an SBOM is, why is it important and that you can download it from an API too. Mention that not only direct dependencies are covered, but all the indirect ones too, this way making transparent and securing your entire software supply chain.

For the license compliance, explain the dangers of accidentally using dependencies with conflicting licenses. Explain how you can detect such problems with this feature before going into production and getting into legal trouble because of license mismatch between your applications and their dependencies.

**Switch to the slides.**

Close the demo with going back to the slides and reiterating on the key messages.

Because the messaging is all about GitLab being the one DevSecOps platform that you can use to do all your software development activities in, a lot of customers get worried that they will be the victim of vendor lock-in and won't be able to use anything else if they invest in GitLab. If this topic comes up, make sure you explain to them that even though you get the best value for your investment if you use GitLab for everything you can, it doesn't mean you cannot integrate GitLab with third-party tools. Reference some of our most well-known integrations, like Jira or Jenkins.

I hope this walkthrough about how to do a GitLab demo was useful for you as a partner. Goodbye and have a nice rest of your day!
